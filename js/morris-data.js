$(function() {

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Perdidos",
            value: 12
        }, {
            label: "Finalizados",
            value: 30
        }, {
            label: "Andamento",
            value: 20
        }],
        resize: true
    });

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: 'Jan',
            a: 100,
            b: 90
        }, {
            y: 'Fev',
            a: 75,
            b: 65
        }, {
            y: 'Mar',
            a: 50,
            b: 40
        }, {
            y: 'Abr',
            a: 75,
            b: 65
        }, {
            y: 'Mai',
            a: 50,
            b: 40
        }, {
            y: 'Jun',
            a: 75,
            b: 65
        }, {
            y: 'Jul',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['2014', '2015'],
        hideHover: 'auto',
        resize: true
    });

});
